package main

import (
	"bufio"
	"encoding/xml"
	"flag"
	"fmt"
	"net/url"
	"os"
	"strings"
)

var (
	flagPrefix = flag.String("prefix", "", "prefix to append to playlist entried")
	flagTitle  = flag.String("title", "playlist", "title of playlist")
)

type XspfPlaylist struct {
	XMLName   xml.Name  `xml:"playlist"`
	NS        string    `xml:"xmlns,attr"`
	Version   int       `xml:"version,attr"`
	Title     string    `xml:"title"`
	TrackList TrackList `xml:"trackList"`
}

type TrackList struct {
	Track []Track `xml:"track"`
}
type Track struct {
	XMLName  xml.Name `xml:"track"`
	Title    string   `xml:"title"`
	Location string   `xml:"location"`
}

func main() {
	flag.Parse()

	fmt.Fprintf(os.Stderr, "reading input from STDIN, hit ^D to finish\n")

	var tracks []Track
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		esc := url.PathEscape(scanner.Text())
		esc = fmt.Sprintf("%s%s", *flagPrefix, strings.ReplaceAll(esc, "%2F", "/"))
		tracks = append(tracks, Track{
			Location: esc,
			Title:    scanner.Text(),
		})
	}

	pl := &XspfPlaylist{
		Title:     *flagTitle,
		NS:        "http://xspf.org/ns/0/",
		Version:   1,
		TrackList: TrackList{Track: tracks},
	}

	b, err := xml.MarshalIndent(pl, " ", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Printf(`<?xml version="1.0" encoding="UTF-8"?>` + "\n")
	fmt.Printf("%s", string(b))
}
